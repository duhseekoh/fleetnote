// @flow
import React, { Component } from 'react';
import styles from './UserSetup.scss';
// import type { User } from '../../../mobx/store/UserStore';

type Props = {
  user: ?Object;
};

class UserSetup extends Component<Props> {
  constructor() {

  }

  render() {
    const { user } = this.props;

    return (
      <div className={styles.userSetupContainer}>
        { user ? (
            <div>
              You're setup as { JSON.stringify(user) }
            </div>
          ) : (
            <div>
              <label>Enter your name to get started.</label>
              <input type='text' />
            </div>
          )
        }
      </div>
    );
  }
}

export default UserSetup;
