// @flow
import React, { Component } from 'react';
import styles from './NoteList.scss';
import NoteItem from './components/NoteItem/NoteItem';
import type { Note } from '../../../mobx/store/NoteStore';

type Props = {
  items: Note[];
};

class NoteList extends Component<Props> {
  constructor() {

  }

  render() {
    const { items } = this.props;

    return (
      <div className={styles.noteListContainer}>
        <h3>Notes</h3>
        { items.map(item =>
          <NoteItem item={item} />
        )}
        <CreateNoteLink />
      </div>
    );
  }
}

const CreateNoteLink = () => (
  <a href={'#'}>+ New Note</a>
);

export default NoteList;
