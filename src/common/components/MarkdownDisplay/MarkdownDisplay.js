// @flow
import React, { Component } from 'react';
import styles from './MarkdownDisplay.scss';

type Props = {
  markdown: string;
};

class MarkdownDisplay extends Component<Props> {
  constructor() {

  }

  render() {
    const { markdown } = this.props;
    return (
      <div className={styles.markdownDisplayContainer}>
        {markdown}
      </div>
    );
  }
}

export default MarkdownDisplay;
