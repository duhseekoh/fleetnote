// @flow
import React, { Component } from 'react';
import styles from './MarkdownField.scss';
import MarkdownDisplay from '../MarkdownDisplay/MarkdownDisplay';

type Props = {

};

type State = {

};

class MarkdownField extends Component<Props> {
  constructor() {

  }

  render() {
    return (
      <div className={styles.markdownFieldContainer}>
        <textarea>

        </textarea>
        {/* TODO Maybe we conditionally display MarkdownDisplay */}
        <MarkdownDisplay
          markdown={"# some markdown"}
        />
      </div>
    );
  }
}

export default MarkdownField;
