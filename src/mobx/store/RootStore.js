// @flow
import { NoteStore } from './NoteStore';
import ApiClient from '../api/ApiClient';

export class RootStore {
  noteStore: NoteStore;

  constructor(api: ApiClient) {
    // TODO this.userStore = new UserStore(this)
    this.noteStore = new NoteStore(this, api);
  }
}
