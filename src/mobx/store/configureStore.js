import { RootStore } from './RootStore';
import ApiClient from '../api/ApiClient';

const api = new ApiClient();
const configureStore = () => new RootStore(api);

export default configureStore();
