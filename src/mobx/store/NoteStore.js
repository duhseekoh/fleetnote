// @flow
import { observable, action, runInAction } from 'mobx';
import type { RootStore } from './RootStore';
import type ApiClient from '../api/ApiClient';

export class NoteStore {
  rootStore: RootStore;
  api: ApiClient;
  @observable notes: Map<string, Note> = new Map();

  constructor(rootStore: RootStore, api: ApiClient) {
    this.rootStore = rootStore;
    this.api = api;
  }

  @action
  async createNote(note: Note) {
    // TODO - call firebase to create a new note
    const createdNote = await this.api.createNote(note);
    runInAction(() => {
      // TODO update the created note here
    });
  }
}

export class Note {
  id: string;
  title: string;
  @observable todos: Todo[] = [];

  constructor() {

  }

  @action
  async save() {
    const savedNote = await this.api.saveNote(this.toJSON());
    runInAction(() => {
      // TODO update the saved note here
    });
  }
}

export class Todo {
  constructor() {

  }
}
