import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import logo from './logo.svg';
import './App.css';
import EntryScreen from './screens/entry/EntryScreen';
import NoteScreen from './screens/note/NoteScreen';

class Routes extends Component {
  render() {
    return (
      // TODO add routes
    )
  }
}

class App extends Component {
  render() {
    return (
      <div className="App">
        {/* <Routes /> */}
        <EntryScreen />
        <NoteScreen />
      </div>
    );
  }
}

export default App;
