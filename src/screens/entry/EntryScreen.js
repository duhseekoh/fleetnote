// @flow
import React, { Component } from 'react';
import styles from './EntryScreen.scss';

type Props = {

};

class EntryScreen extends Component<Props> {
  render() {
    return (
      <div className={styles.screenContainer}>
        {/* <label>Who are you?</label>
        <input type='text'></input> */}
        <a href="#">Add a note</a>
        
      </div>
    );
  }
}

export default EntryScreen;
