// @flow
import React, { Component } from 'react';
import styles from './NoteScreen.scss';
import NoteDescription from './components/NoteDescription/NoteDescription';
import NoteTitle from './components/NoteTitle/NoteTitle';
import NoteToolbar from './components/NoteToolbar/NoteToolbar';
import TodoList from './components/TodoList/TodoList';

type OwnProps = {

};

type ConnectedProps = {

};

type Props = OwnProps & ConnectedProps;

class NoteScreen extends Component<Props> {
  constructor() {

  }

  render() {
    return (
      <div className={styles.noteContainer}>
        <NoteToolbar
          isDirty={true}
          onPressSave={() => alert('pressed save')}
          onPressDelete={() => alert('pressed delete')}
        />
        <NoteTitle
          text={'Note Title'}
        />
        <NoteDescription
          text={'Note Description'}
        />
        <TodoList
          items={[]}
        />
      </div>
    );
  }
}

export default NoteScreen;
