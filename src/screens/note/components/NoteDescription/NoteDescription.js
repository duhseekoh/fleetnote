// @flow
import React, { Component } from 'react';
import styles from './NoteDescription.scss';

type Props = {
  text: string,
};

class NoteDescription extends Component<Props> {
  constructor() {

  }

  render() {
    const { text } = this.props;
    return (
      <div className={styles.noteDescriptionContainer}>
        {text}
      </div>
    );
  }
}

export default NoteDescription;
