// @flow
import React, { Component } from 'react';
import styles from './NoteToolbar.scss';

type Props = {
  isDirty: boolean;
  onPressSave: () => any;
  onPressDelete: () => any;
};

class NoteToolbar extends Component<Props> {
  constructor() {

  }

  render() {
    const { isDirty, onPressSave, onPressDelete } = this.props;

    return (
      <div className={styles.toolbarContainer}>
        <div>
          { isDirty ?
            <div>Unsaved Changes</div>
            :
            <div>Saved</div>
          }
        </div>
        <button onClick={onPressSave}>Save</button>
        <button onClick={onPressDelete}>Delete</button>
      </div>
    );
  }
}

export default NoteToolbar;
