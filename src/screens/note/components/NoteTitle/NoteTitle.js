// @flow
import React, { Component } from 'react';
import styles from './NoteTitle.scss';

type Props = {
  text: string,
};

class NoteTitle extends Component<Props> {
  constructor() {

  }

  render() {
    const { text } = this.props;
    return (
      <div className={styles.noteTitleContainer}>
        {text}
      </div>
    );
  }
}

export default NoteTitle;
