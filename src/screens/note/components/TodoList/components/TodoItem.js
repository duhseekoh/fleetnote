// @flow
import React, { Component } from 'react';
import styles from './TodoItem.scss';
import type { Todo } from '../../../../../mobx/store/NoteStore';

type Props = {
  item: Todo;
};

class TodoItem extends Component<Props> {
  constructor() {

  }

  render() {
    const { item } = this.props;

    return (
      <div className={styles.todoContainer}>
        {/*
          Should consist of
          - mark complete component
          - display only component
          - edit component
          - completed by person component
        */}
        {JSON.stringify(item)}
      </div>
    );
  }
}

export default TodoItem;
