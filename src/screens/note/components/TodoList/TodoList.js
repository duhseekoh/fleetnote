// @flow
import React, { Component } from 'react';
import styles from './TodoList.scss';
import TodoItem from './components/TodoItem';
import type { Todo } from '../../../../mobx/store/NoteStore';

type Props = {
  items: Todo[];
};

class TodoList extends Component<Props> {
  constructor() {

  }

  render() {
    const { items } = this.props;

    return (
      <div className={styles.todoListContainer}>
        <h3>Tasks</h3>
        { items.map(item =>
          <TodoItem item={item} />
        )}
      </div>
    );
  }
}

export default TodoList;
