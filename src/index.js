import * as firebase from 'firebase';
import { Provider } from 'mobx-react';
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import configureStore from './mobx/store/configureStore';

const firebaseApp = firebase.initializeApp({

});

const rootStore = configureStore();

ReactDOM.render(
  <Provider rootStore={rootStore}>
    <App />
  </Provider>, document.getElementById('root')
);
